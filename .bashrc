
alias gui='cd ~/Desktop/tbl/lgames-gui'
alias api='cd ~/Desktop/tbl/djreact'
alias mat='cd ~'

alias vimb='vim ~/.bashrc'
alias vimv='vim ~/.vimrc'

alias sb='source ~/.bashrc'
alias sv='source ~/.vimrc'

alias env='source env/bin/activate'

alias gp='git pull ; git push'
alias gs='git status'
alias ga='git add -A'
alias gd='git diff'
alias gc='git commit -m'

alias rs='python3 manage.py runserver'
alias ns='npm start'

alias caen='ssh wnobrien@login.engin.umich.edu'

alias 482='cd ~/Desktop/class/eecs/482'
alias 481='cd ~/Desktop/class/eecs/481'
alias silas='cd ~/Desktop/misc/silas/coleman_properties'

alias ls='ls -G'
