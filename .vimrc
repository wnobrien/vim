
syntax on

set path+=**
set autoindent
set nocompatible
set number
set tabstop=4
set sw=4
set backspace=indent,eol,start
set selectmode=mouse
set laststatus=2
set showmatch
set history=100

inoremap jk <ESC>

vnoremap jk <ESC>

nnoremap zz :w<CR>

"Alias for CTRL-n"
inoremap ;; <C-n>

"Easier copy/pasting from visual mode to system clipboard"
vnoremap qq :w !pbcopy<CR><CR>

"C++ specifics"
inoremap {<CR> {<CR>}<ESC>ko<TAB>
nnoremap -+- yyGp$x$a<CR>{<CR>}<ESC>ko<TAB>

"Map ENTER to add a newline in normal mode"
nnoremap <CR> A<CR><ESC>

"Sublime colors... fuck ya"
colo sublimemonokai
